import {api} from "@lib/api";
import {TCoffee} from "@lib/types";

const get = async () => {
  try {
    const res = await api.get<TCoffee>('/');

    return res.data;
  } catch (e) {
    console.log(e, 'Send log to stripe')
  }
};


const coffeeApi = {
  get,
}


export {
  coffeeApi,
}

import React from 'react'
import ReactDOM from 'react-dom';
import {RecoilRoot} from "recoil";

import './index.css';


import {Coffee} from "@components/domains/coffee";

console.log('Сборка прошла')

ReactDOM.render(
  <React.StrictMode>
    <RecoilRoot>
      <Coffee/>
    </RecoilRoot>
  </React.StrictMode>,
  document.getElementById('root')
);

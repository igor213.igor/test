import React from "react";


type TDivProps = Partial<React.ReactHTMLElement<HTMLDivElement>>
type TImgProps = Partial<React.ReactHTMLElement<HTMLImageElement>>
type TButtonProps = Partial<React.ReactHTMLElement<HTMLButtonElement>>

type THTMLProps<T extends HTMLElement> = Partial<React.ReactHTMLElement<T>>

export type {
  TDivProps,
  TImgProps,
  TButtonProps,
  THTMLProps,
}

import './style.css';
import React, {useEffect} from "react";

import {CoffeeList} from "@components/domains/coffee/CoffeeList";
import {useDataLoad} from "@components/domains/coffee/hooks/useDataLoad";
import {useUserActive} from "@components/domains/utils/hooks/useUserActive";
import {Overlay} from "@components/domains/utils/Overlay";


type TCoffeeProps = Partial<React.ReactHTMLElement<HTMLDivElement>> & {};

const Coffee = (props: TCoffeeProps): React.ReactElement | null => {
  const {
    ...other
  } = props;

  const {handleLoadAndSet} = useDataLoad();

  useUserActive(30000, () => handleLoadAndSet())

  useEffect(() => {

    handleLoadAndSet()

  }, [])


  return (
    <div
      className="coffee-page"
      {...other}
    >
      <Overlay/>
      <CoffeeList/>
    </div>
  )
}


export {
  Coffee,
}

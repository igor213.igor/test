import {useState} from 'react'
import {useRecoilState, useSetRecoilState} from "recoil";
import {coffeeState} from "Recoil/coffee/state";

import {coffeeApi} from "@api/coffee";

import {domHelpers} from "@lib/helpers/dom";
import {TCoffee} from "@lib/types";


const useDataLoad = () => {
  const setCoffee = useSetRecoilState(coffeeState);
  const [loading, setLoading] = useState(false);


  const handleScrollBottom = () => {
    const scrollElement = domHelpers.getElement('.coffee-page .coffee-list-content');

    if(!scrollElement){
      return;
    }
    domHelpers.scrollToFromElement(scrollElement, 0, scrollElement.scrollHeight)
  }

  const handleSet = (data: TCoffee) => {
    setCoffee((oldCoffee) =>
      oldCoffee.concat(data)
    );
  }

  const handleLoadAndSet = async () => {
    setLoading(true)
    const data = await coffeeApi.get();

    if (!data) {
      return;
    }

    setLoading(false)

    handleSet(data)

    handleScrollBottom()
  }


  return {
    handleLoadAndSet,
    loading,
  }
}

export {
  useDataLoad,
}

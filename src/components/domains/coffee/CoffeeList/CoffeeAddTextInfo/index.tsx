import React from "react";

import {THTMLProps} from "@components/domains/types";


type TCoffeeAddTextInfoProps = THTMLProps<HTMLParagraphElement> & {}

const CoffeeAddTextInfo = (props: TCoffeeAddTextInfoProps): React.ReactElement | null => {
  const {
    ...other
  } = props;

  return (
    <p
      {...other}>
      Click to load the following
    </p>
  )
}


export {
  CoffeeAddTextInfo,
}

import {coffeeSelectors} from "@Recoil/coffee/selectors";
import React from "react";
import {useRecoilValue} from "recoil";

import {IconCoffeeVariety} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeVariety/Icon";
import {TDivProps} from "@components/domains/types";

type TCoffeeVarietyProps = TDivProps & {
  id: number
}

const CoffeeVariety = (props: TCoffeeVarietyProps): React.ReactElement | null => {
  const {
    id,
    ...other
  } = props;

  const coffeeVariety = useRecoilValue(coffeeSelectors.getVarietyById(id));

  return (
    <div
      className="item-variety"
      {...other}>

      <IconCoffeeVariety/>

      <p>
        {coffeeVariety}
      </p>

    </div>
  )
}


export {
  CoffeeVariety,
}

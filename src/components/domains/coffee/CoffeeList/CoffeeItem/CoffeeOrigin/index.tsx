import {coffeeSelectors} from "@Recoil/coffee/selectors";
import React from "react";
import {useRecoilValue} from "recoil";

import { TDivProps } from "@components/domains/types";


type TCoffeeOriginProps = TDivProps & {
  id:number
}

const CoffeeOrigin = (props: TCoffeeOriginProps): React.ReactElement | null => {
  const {
    id,
    ...other
  } = props;

  const coffeeOrigin = useRecoilValue(coffeeSelectors.getOriginById(id));


  return (
    <p
      className="item-origin"
      {...other}>
      {coffeeOrigin}
    </p>
  )
}


export {
  CoffeeOrigin,
}

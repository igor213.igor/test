import {coffeeSelectors} from "@Recoil/coffee/selectors";
import React from "react";
import {useRecoilValue} from "recoil";

import {TDivProps} from "@components/domains/types";


type TCoffeeNotesProps = TDivProps & {
  id: number
}

const CoffeeNotes = (props: TCoffeeNotesProps): React.ReactElement | null => {
  const {
    id,
    ...other
  } = props;

  const coffeeNotes = useRecoilValue(coffeeSelectors.getNotesById(id));

  const renderContent = () => {
    if (!coffeeNotes) {
      return null;
    }

    const arrCoffeeNotes = coffeeNotes.split(', ');


    return arrCoffeeNotes.map((itemNote,idx) => (
      <div
        key={idx.toString()}
        className="notes-item">
        {
          itemNote
        }
      </div>
    ))

  }

  return (
    <div
      className="item-notes"
      {...other}>
      {
        renderContent()
      }
    </div>
  )
}


export {
  CoffeeNotes,
}

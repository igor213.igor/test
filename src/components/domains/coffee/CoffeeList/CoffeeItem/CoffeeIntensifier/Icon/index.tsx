import { ReactComponent as IntensifierComponent} from './Icon.svg';


const IconCoffeeIntensifier = () => {
  return(
    <IntensifierComponent/>
  )
}

export {
  IconCoffeeIntensifier,
}

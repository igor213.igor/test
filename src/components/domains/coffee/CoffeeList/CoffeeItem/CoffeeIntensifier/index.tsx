import {coffeeSelectors} from "@Recoil/coffee/selectors";
import React from "react";
import {useRecoilValue} from "recoil";

import {IconCoffeeIntensifier} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeIntensifier/Icon";
import {TDivProps} from "@components/domains/types";

type TCoffeeIntensifierProps = TDivProps & {
  id: number;
}

const CoffeeIntensifier = (props: TCoffeeIntensifierProps): React.ReactElement | null => {
  const {
    id,
    ...other
  } = props;

  const coffeeIntensifier = useRecoilValue(coffeeSelectors.getIntensifierById(id));

  return (
    <div
      className="item-intensifier"
      {...other}>
      <IconCoffeeIntensifier/>
      <p>
        {coffeeIntensifier}
      </p>
    </div>
  )
}


export {
  CoffeeIntensifier,
}

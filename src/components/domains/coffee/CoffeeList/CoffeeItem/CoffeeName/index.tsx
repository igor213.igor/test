import {coffeeSelectors} from "@Recoil/coffee/selectors";
import React from "react";
import {useRecoilValue} from "recoil";

import {TDivProps} from "@components/domains/types";


type TCoffeeNameProps = TDivProps & {
  id: number
}

const CoffeeName = (props: TCoffeeNameProps): React.ReactElement | null => {
  const {
    id,
    ...other
  } = props;


  const coffeeName = useRecoilValue(coffeeSelectors.getNameById(id));

  return (
    <p
      className="item-name"
      {...other}>
      {coffeeName}
    </p>
  )
}


export {
  CoffeeName,
}

import {coffeeSelectors} from "@Recoil/coffee/selectors";
import React from "react";
import {useRecoilValue} from "recoil";

import {CoffeeImg} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeImg";
import {CoffeeIntensifier} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeIntensifier";
import {CoffeeName} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeName";
import {CoffeeNotes} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeNotes";
import {CoffeeOrigin} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeOrigin";
import {CoffeeVariety} from "@components/domains/coffee/CoffeeList/CoffeeItem/CoffeeVariety";
import type {TDivProps} from "@components/domains/types";

type TCoffeeItemProps = TDivProps & {
  id: number
}

const CoffeeItem = (props: TCoffeeItemProps): React.ReactElement | null => {
  const {
    id,
    ...other
  } = props;

  const hasCoffee = useRecoilValue(coffeeSelectors.hasById(id));

  if (!hasCoffee) {

    return null

  }

  return (
    <div
      className="coffee-list-item"
      {...other}>
      <div
        className="item-top-block">

        <CoffeeImg/>

      </div>
      <div
        className="item-bottom-block">

        <CoffeeName
          id={id}
        />

        <CoffeeOrigin
          id={id}
        />

        <CoffeeNotes
          id={id}
        />

        <div
          className="last-block">

          <CoffeeVariety
            id={id}
          />

          <CoffeeIntensifier
            id={id}
          />

        </div>

      </div>
    </div>
  )
}


export {
  CoffeeItem,
}

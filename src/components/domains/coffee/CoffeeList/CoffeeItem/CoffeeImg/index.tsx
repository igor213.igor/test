import React from "react";

import {TDivProps} from "@components/domains/types";
import {Loading} from "@components/domains/utils/Loading";


type TCoffeeImgProps = TDivProps & {}

const CoffeeImg = (props: TCoffeeImgProps): React.ReactElement | null => {
  const {
    ...other
  } = props;

  return (
    <div
      {...other}>
      <Loading/>
      <img
        className="item-img"
        src="https://loremflickr.com/500/500/coffee%20bean"
        loading="lazy"
      />
    </div>
  )
}


export {
  CoffeeImg,
}

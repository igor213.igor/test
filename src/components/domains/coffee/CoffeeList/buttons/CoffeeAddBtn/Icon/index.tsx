import { ReactComponent as AddComponent} from './Icon.svg';


const IconCoffeeAdd = () => {
  return(
    <AddComponent/>
  )
}

export {
  IconCoffeeAdd,
}

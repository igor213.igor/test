import './style.css';
import React from "react";

import {IconCoffeeAdd} from "@components/domains/coffee/CoffeeList/buttons/CoffeeAddBtn/Icon";
import {useDataLoad} from "@components/domains/coffee/hooks/useDataLoad";


const CoffeeAddBtn = (): React.ReactElement | null => {

  const {handleLoadAndSet, loading} = useDataLoad();


  const handleClick = async () => {
    if (loading) {
      return;
    }

    await handleLoadAndSet()
  }

  return (
    <button
      className="coffee-add-btn"
      onClick={handleClick}
      disabled={loading}
    >
      <IconCoffeeAdd/>
    </button>
  )
}


export {
  CoffeeAddBtn,
}

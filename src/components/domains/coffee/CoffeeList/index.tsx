import React from "react";
import {useRecoilValue} from "recoil";
import {coffeeSelectors} from "Recoil/coffee/selectors";

import {CoffeeAddBtn} from "@components/domains/coffee/CoffeeList/buttons/CoffeeAddBtn";
import {CoffeeAddTextInfo} from "@components/domains/coffee/CoffeeList/CoffeeAddTextInfo";
import {CoffeeItem} from "@components/domains/coffee/CoffeeList/CoffeeItem";
import type {TDivProps} from "@components/domains/types";


type TCoffeeListProps = TDivProps & {}

const CoffeeList = (props: TCoffeeListProps): React.ReactElement | null => {
  const {
    ...other
  } = props;


  const ids = useRecoilValue(coffeeSelectors.getIds);

  if (!ids.length) {
    return null;
  }


  return (
    <div
      className="coffee-list"
      {...other}>
      <div
        className="coffee-list-content">
        {
          ids.map(id =>
            <CoffeeItem
              key={id.toString()}
              id={id}
            />
          )
        }
      </div>
      <div
        className="coffee-list-add-block">
        <CoffeeAddBtn/>
        <CoffeeAddTextInfo/>
      </div>
    </div>
  )
}


export {
  CoffeeList,
}

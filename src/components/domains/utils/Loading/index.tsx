import React from "react"

import {TImgProps} from "@components/domains/types";

const Loading = (props: TImgProps): React.ReactElement => {
  const {
    ...other
  } = props;

  return (
    <img
      src="/images/loading.gif"
      {...other}
    />
  )
}


export {
  Loading,
}

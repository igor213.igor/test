import './style.css';
import React from "react"

import {TImgProps} from "@components/domains/types";

const Overlay = (props: TImgProps): React.ReactElement => {
  const {
    ...other
  } = props;

  return (
    <img
      className="overlay-coffee"
      src="/images/overlay.jpg"
      {...other}
    />
  )
}


export {
  Overlay,
}

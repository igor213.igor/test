import {useEffect} from "react";

import {useEventListener} from "@components/domains/utils/hooks/useEventListener";
import {useInterval} from "@components/domains/utils/hooks/useInterval";

const useUserActive = (timeOut: number, cb = (): void => {
}) => {

  let lastActiveDateMls = 0;
  let isActive = false;

  const checkIsActive = () => {
    if (isActive && new Date().getTime() - lastActiveDateMls > timeOut) {
      isActive = false;
      cb()
    }
  }

  const setIsActive = () => {
    lastActiveDateMls = new Date().getTime();
    isActive = true;
  }

  useEventListener(
    document,
    'mousemove',
    setIsActive
  );

  useEventListener(
    document,
    'click',
    setIsActive
  );

  useEventListener(
    document,
    'mousemove',
    setIsActive
  );

  useInterval(
    'interval',
    () => {
      checkIsActive()
    },
    3000
  );

  useEffect(() => {
    setIsActive();
  })


}

export {
  useUserActive,
}

import {atom} from "recoil";

import {TCoffeeState, KEY_STATE} from "./types";

const coffeeState = atom<TCoffeeState>({
  key: KEY_STATE,
  default: [],
});

export {
  coffeeState,
}

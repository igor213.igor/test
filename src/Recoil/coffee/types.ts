import {TCoffee} from "@lib/types";

const KEY_STATE = 'coffeeState';

enum KEY_SELECTORS {
  HAS_BY_ID = 'hasById',
  GET_IDS = 'getIds',
  GET_STATE = 'getState',
  GET_BY_ID = 'getById',
  GET_NAME_BY_ID = 'getNameById',
  GET_NOTES_BY_ID = 'getNotesById',
  GET_INTEN_BY_ID = 'getIntenById',
  GET_ORIGIN_BY_ID = 'getOriginById',
  GET_VARIETY_BY_ID = 'getVarietyById',
}

type TCoffeeState = TCoffee[];


export {
  KEY_STATE,
  KEY_SELECTORS,
}

export type {
  TCoffeeState,
}

import {selector, selectorFamily} from "recoil";

import {coffeeState} from "./state";
import {KEY_SELECTORS} from "./types";


const getIds = selector({
  key: KEY_SELECTORS.GET_IDS,
  get: ({get}) => {
    const data = get(coffeeState);

    return data.map(el => el.id);
  },
});


const getById = selectorFamily({
  key: KEY_SELECTORS.GET_BY_ID,
  get: (id: number) => ({get}) => {
    const data = get(coffeeState);

    return data.find(el => el.id === id);
  },
});


const hasById = selectorFamily({
  key: KEY_SELECTORS.HAS_BY_ID,
  get: (id: number) => ({get}) => {
    const data = get(getById(id));

    return !!data
  },
});

const getNameById = selectorFamily({
  key: KEY_SELECTORS.GET_NAME_BY_ID,
  get: (id: number) => ({get}) => {
    const data = get(getById(id));

    if (!data) {
      return false;
    }

    return data.blend_name
  },
});


const getNotesById = selectorFamily({
  key: KEY_SELECTORS.GET_NOTES_BY_ID,
  get: (id: number) => ({get}) => {
    const data = get(getById(id));

    if (!data) {
      return '';
    }

    return data.notes
  },
});

const getIntensifierById = selectorFamily({
  key: KEY_SELECTORS.GET_INTEN_BY_ID,
  get: (id: number) => ({get}) => {
    const data = get(getById(id));

    if (!data) {
      return '';
    }

    return data.intensifier
  },
});

const getOriginById = selectorFamily({
  key: KEY_SELECTORS.GET_ORIGIN_BY_ID,
  get: (id: number) => ({get}) => {
    const data = get(getById(id));

    if (!data) {
      return '';
    }

    return data.origin
  },
});

const getVarietyById = selectorFamily({
  key: KEY_SELECTORS.GET_VARIETY_BY_ID,
  get: (id: number) => ({get}) => {
    const data = get(getById(id));

    if (!data) {
      return '';
    }

    return data.variety
  },
});

const coffeeSelectors = {
  getIds,
  getById,
  hasById,
  getNameById,
  getNotesById,
  getIntensifierById,
  getOriginById,
  getVarietyById,
}

export {
  coffeeSelectors,
}

const getElement = (selector: string) => document.querySelector(selector)
const scrollToFromElement = (element: Element, from: number, to: number) => element.scrollTo(from, to)

const domHelpers = {
  getElement,
  scrollToFromElement,
}

export {
  domHelpers,
}

import axios from "axios";


const baseUrl = 'https://random-data-api.com/api/coffee/random_coffee';

const api = axios.create({
  baseURL: `${baseUrl}`,
  timeout: 5000,
  maxRedirects: 2,
});


export {
  api,
}

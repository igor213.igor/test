type TCoffee = {
  blend_name: string;
  id: number;
  intensifier: string;
  notes: string;
  origin: string;
  uid: string;
  variety: string
}


export type {
  TCoffee,
}
